import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,

  };
  
  handleSubmit=event=>{
    if(event.key==='Enter') {
      const newArr = {
        userId: 1,
        id: Math.round(Math.random()*1000),
        title: event.target.value,
        completed: false 
      }
        const newArrs = this.state.todos.slice()
        newArrs.push(newArr)
        this.setState({todos:newArrs})
        event.target.value=''
        }
      }
 
  toggleCheckChange = (event, idCheck) => {
     const newTodo = this.state.todos.slice()
     const newTodos = newTodo.map(todo=>{
        if(todo.id === idCheck){
          todo.completed = !todo.completed
        }
          return todo})
        this.setState({todos:newTodos})
  }

  deleteTodo = (event, toDelete) => {
    console.log('ran')
    const newTodoList = this.state.todos.filter(todo=>{
       if(todo.id === toDelete ){
         return false 
       }
         return true})
       this.setState({todos:newTodoList})
       
 }

 deleteAllCompleted = () => {
  const newTodoList = this.state.todos.filter(todo=>{
     if(todo.completed === true){
       return false 
     }
       return true})
     this.setState({todos:newTodoList})
     
}

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
             <input className="new-todo"
             onKeyDown={this.handleSubmit}
             onChange={this.toggleCheckChange}
            placeholder="What needs to be done?" autoFocus />       
        </header>
        <TodoList todos={this.state.todos} 
            toggleChange={this.toggleCheckChange} 
            deleteTodo={this.deleteTodo}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed"
          onClick={this.deleteAllCompleted}
          >Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" 
              type="checkbox" 
              checked={this.props.completed} 
              onChange={(event)=>(this.props.toggleCheckChange(event, this.props.id))}
          />
          <label>{this.props.title}</label>
          <button className="destroy" 
          onClick={(event)=>this.props.deleteItem(event, this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              title={todo.title} 
              completed={todo.completed} 
              id={todo.id}
              key={todo.id}
              toggleCheckChange={this.props.toggleChange}
              deleteItem={this.props.deleteTodo}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
